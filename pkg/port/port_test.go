package port

import (
	"encoding/json"
	"testing"
)

// test NewPort() function
func TestNewPort(t *testing.T) {
	p := new(Port)
	p.NewPort("12345", "Port Name", "Country Name", []string{"alias1", "alias2"}, []float64{1.0, 2.0}, "province", "timezone", []string{"unloc1", "unloc2"}, "code")
	if p.ID != "12345" {
		t.Error("expected 12345")
	}
	if p.Name != "Port Name" {
		t.Error("expected Port Name")
	}
	if p.Country != "Country Name" {
		t.Error("expected Country Name")
	}
	if p.Alias[0] != "alias1" {
		t.Error("expected alias1")
	}
	if p.Coordinates[0] != 1.0 {
		t.Error("expected 1.0")
	}
	if p.Province != "province" {
		t.Error("expected province")
	}
	if p.Timezone != "timezone" {
		t.Error("expected timezone")
	}
	if p.Unlocs[0] != "unloc1" {
		t.Error("expected unloc1")
	}
	if p.Code != "code" {
		t.Error("expected code")
	}
}

func TestDecoder(t *testing.T) {
	portJson := `{"AEAJM":{"name": "Ajman","city": "Ajman","country": "United Arab Emirates","alias": [],"regions": [],	"coordinates": [55.5136433, 25.4052165],"province": "Ajman","timezone": "Asia/Dubai","unlocs": [ "AEAJM"],"code": "52000"},"BBC":{"name": "Ajman","city": "Ajman","country": "United Arab Emirates","alias": [],"regions": [],	"coordinates": [55.5136433, 25.4052165],"province": "Ajman","timezone": "Asia/Dubai","unlocs": [ "AEAJM"],"code": "52000"}}`
	var port = make(map[string]Port)
	json.Unmarshal([]byte(portJson), &port)

	if port["BBC"].Name != "Ajman" {
		t.Error("expected Ajman")
	}

	p := new(Port)
	mock := `{"name": "Ajman","city": "Ajman","country": "United Arab Emirates","alias": [],"regions": [],	"coordinates": [55.5136433, 25.4052165],"province": "Ajman","timezone": "Asia/Dubai","unlocs": [ "AEAJM"],"code": "52000"}`
	err := p.Decoder([]byte(mock))
	if err != nil {
		t.Error(err)
	}
	if p.Name != "Ajman" {
		t.Error("expected Ajman")
	}
	if p.Country != "United Arab Emirates" {
		t.Error("expected United Arab Emirates")
	}
	if p.Coordinates[0] != 55.5136433 {
		t.Error("expected 55.5136433")
	}
	if p.Province != "Ajman" {
		t.Error("expected Ajman")
	}
}
