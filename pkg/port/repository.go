package port

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"log"
	pg "ports/internal/db/postgres"
)

type pgPool struct {
	db *pgxpool.Pool
}

func NewPGRepository(db *pgxpool.Pool) *pgPool {
	return &pgPool{
		db: db,
	}
}

func (p *pgPool) Save(ctx context.Context, port Port) (id string, err error) {
	// saving with pessimistic concurrency control
	tx, err := p.db.BeginTx(ctx, pgx.TxOptions{IsoLevel: "serializable"})
	if err != nil {
		log.Print("\n[ERROR]: TRANSACTION COULD NOT BEGIN")
		return "", fmt.Errorf("port repository save: %w", err)
	}
	defer tx.Rollback(ctx)

	tsql := `
		 INSERT INTO port (
				  id,
				  name, 
				  country, 
				  alias,
				  coordinates,
				  province,
				  timezone,
				  unlocs,
				  code
				  )
		 VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)
         
		 ON CONFLICT (id, version) 
		 DO UPDATE SET 
			  version = EXCLUDED.version,
			  name = EXCLUDED.name,
			  country = EXCLUDED.country,
			  alias = EXCLUDED.alias,
			  coordinates = EXCLUDED.coordinates,
			  province = EXCLUDED.province,
			  timezone = EXCLUDED.timezone,
			  unlocs = EXCLUDED.unlocs,
			  code = EXCLUDED.code

		 RETURNING id;
`
	// QueryRow is used instead of Exec because of postgres returning property
	err = tx.QueryRow(ctx, tsql,
		port.ID,
		port.Name,
		port.Country,
		port.Alias,
		port.Coordinates,
		port.Province,
		port.Timezone,
		port.Unlocs,
		port.Code,
	).Scan(&id)
	if err != nil {
		pg.RollbackTxPgx(tx, err)
		log.Print("\n[ERROR]: TRANSACTION ERROR")
		return "", fmt.Errorf("port repository save: %w", err)
	}

	err = tx.Commit(ctx)
	if err != nil {
		log.Print("\n[ERROR]: TRANSACTION COULD NOT COMMIT \n")
		return "", fmt.Errorf("port repository save: %w", err)
	} else {
		// TODO: Caching
	}
	return id, nil
}
