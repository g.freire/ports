package port

import (
	"log"
	"ports/internal/config"
	"ports/internal/constant"
	pg "ports/internal/db/postgres"
	"sync"
	"testing"
)

// create a test function for NewPortStream() function
func TestDecodeStream(t *testing.T) {
	conf := config.GetConfig()
	log.Print(constant.Green + "LOAD CONFIG" + constant.Reset)

	var singleton sync.Once
	db, err := pg.NewPostgresConnectionPool(&singleton, conf.PostgresHost)
	defer db.Conn.Close()
	if err != nil {
		t.Error(err)
	}
	portRepository := NewPGRepository(db.Conn)

	// create a new PortStream
	wg := &sync.WaitGroup{}
	wg.Add(1)
	s := NewPortStream(wg, portRepository, 0)

	// check if the stream is nil
	if s.stream == nil {
		t.Error("stream should not be null")
	}
}
