package port

import (
	"context"
	"fmt"
	"log"
	"sync"
)

func (s *PortStream) processJob(wg *sync.WaitGroup, worker int) {
	defer wg.Done()

	for p := range s.jobsChan {
		err := s.SavePortAtDatabase(p, false)
		if err != nil {
			log.Print(err)
		}
		fmt.Printf("Worker %d processed job id %s\n", worker, p.ID)
	}

}

// manages the workers pool
func (s *PortStream) startProcess(ctx context.Context) {
	for {
		select {
		case job := <-s.stream:
			s.jobsChan <- job

		case <-ctx.Done():
			fmt.Println("Consumer received cancellation signal, closing jobsChan!")
			close(s.jobsChan)
			fmt.Println("Consumer closed jobsChan")
			return
		}

	}
}

// manages the workers pool w/ nested select
func (s *PortStream) startProcessNested(ctx context.Context) {
	defer close(s.jobsChan)

	for {
		select {
		case job := <-s.stream:
			select {
			case <-ctx.Done():
				return
			default:
			}
			s.jobsChan <- job

		case <-ctx.Done():
			fmt.Println("Consumer received cancellation signal, closing jobsChan!")
			fmt.Println("Consumer closed jobsChan")
			return
		}
	}
}
