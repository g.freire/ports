package port

import (
	"context"
	"fmt"
	"log"
)

func (s PortStream) SavePortAtDatabase(port Port, logger bool) error {
	id, err := s.repository.Save(context.Background(), port)
	if err != nil {
		return fmt.Errorf("SavePortAtDatabase: %w", err)
	}
	if logger {
		log.Printf("Saved port with id: %s at postgres", id)
	}
	return nil
}
