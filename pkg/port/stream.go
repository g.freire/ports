package port

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"sync"
)

// Stream helps transmit each streams withing a channel.
type PortStream struct {
	stream     chan Port
	jobsChan   chan Port
	repository *pgPool
	wg         *sync.WaitGroup
}

// NewPortStream constructor returns a new `Stream` type.
func NewPortStream(wg *sync.WaitGroup, pgPool *pgPool, workers int) PortStream {
	p := PortStream{
		stream:     make(chan Port),
		jobsChan:   make(chan Port, workers),
		repository: pgPool,
		wg:         wg,
	}
	for i := 0; i < workers; i++ {
		p.wg.Add(1)
		go p.processJob(wg, i)
	}
	return p
}

// Watch watches JSON streams. Each stream entry will have a Port object.
func (s PortStream) WatchPortStream() <-chan Port {
	return s.stream
}

func (s PortStream) Shutdown() {
	close(s.stream)
}

// Start starts streaming JSON file line by line. If an error occurs, the channel
// will be closed.
func (s PortStream) DecodeStream(ctx context.Context, file *os.File, logger, concurrent bool) error {

	// If concurrent is true, we will concurrently write to db and read from json
	if concurrent {
		go s.startProcessNested(ctx)
		defer s.Shutdown()
	}

	// Start decoding
	dec := json.NewDecoder(file)
	// Expects object as the first token.
	t, err := dec.Token()
	if err != nil {
		return fmt.Errorf("decode stream: %w", err)
	}
	if t != json.Delim('{') {
		return fmt.Errorf("expected {, got %v", t)
	}

	// Read file content as long as there is something in the JSON stream
	i := 1
	for dec.More() {
		// Read the key.
		t, err = dec.Token()
		if err != nil {
			return fmt.Errorf("decode stream: %w", err)
		}
		key := t.(string) // type assert token to string.

		// Decode the value into the port struct
		var port Port
		if err := dec.Decode(&port); err != nil {
			return fmt.Errorf("decode stream: %w", err)
		}
		port.ID = key

		if logger {
			log.Printf("key %q, line %d\n", key, i)
		}

		// Saves at the repository
		if !concurrent {
			err = s.SavePortAtDatabase(port, true)
			if err != nil {
				return err
			}
		} else {
			// send the chunkSize to the channel to be saved by the goroutine
			s.stream <- port
		}
		i++
	}
	fmt.Printf("Total lines: %d\n", i)

	return nil
}
