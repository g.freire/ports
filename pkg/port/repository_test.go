package port

import (
	"context"
	"log"
	"ports/internal/config"
	pg "ports/internal/db/postgres"
	"sync"
	"testing"
	"time"
)

// test seed function
func TestSeed(t *testing.T) {
	log.Println("TestSeed")
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, 15*time.Second)
	defer cancel()

	// Config loads info from env files
	conf := config.GetConfig()
	var singleton sync.Once
	db, err := pg.NewPostgresConnectionPool(&singleton, conf.PostgresHost)
	if err != nil {
		t.Error(err)
	}
	err = pg.ResetDB(ctx, db.Conn)
	if err != nil {
		t.Error(err)
	}
	//defer db.Close()

	sqlMigration := `
			CREATE TABLE IF NOT EXISTS port(
				id VARCHAR(5) NOT NULL,
				name VARCHAR(80) NOT NULL,
				country VARCHAR(60) NOT NULL,
				alias jsonb NULL,
				coordinates jsonb NULL,
				province VARCHAR(60) NOT NULL,
				timezone VARCHAR(80) NOT NULL,
				unlocs jsonb NULL,
				code VARCHAR(80) NULL,
				creation_time TIMESTAMP NOT NULL DEFAULT NOW(),
				version INTEGER NOT NULL DEFAULT 1,
				PRIMARY KEY(id, version)
			);`
	if _, err := db.Conn.Exec(context.Background(), sqlMigration); err != nil {
		t.Error("could not create port table")
	}

	sql := "SELECT COUNT(*) FROM port"
	if _, err := db.Conn.Exec(context.Background(), sql); err != nil {
		t.Error("expected the metadata table")
	}
}

// test save function
func TestSave(t *testing.T) {
	log.Println("TestSave")
	ctx := context.Background()
	//ctx, cancel := context.WithTimeout(ctx, 15*time.Second)
	//defer cancel()

	m := Port{
		ID:          "12345",
		Name:        "Port Name",
		Country:     "Country Name",
		Alias:       []string{"alias1", "alias2"},
		Coordinates: []float64{1.0, 2.0},
		Province:    "Province Name",
		Timezone:    "Timezone Name",
		Unlocs:      []string{"unloc1", "unloc2"},
		Code:        "Code Name",
	}

	// Config loads info from env files
	conf := config.GetConfig()
	var singleton sync.Once
	db, err := pg.NewPostgresConnectionPool(&singleton, conf.PostgresHost)
	if err != nil {
		t.Error(err)
	}
	//defer db.Close()

	p := NewPGRepository(db.Conn)
	id, err := p.Save(ctx, m)
	if err != nil {
		t.Error(err)
	}
	if id != "12345" {
		t.Error("expected id to be 12345")
	}
}
