package port

import (
	"encoding/json"
	"fmt"
)

// Port represents the port object.
type Port struct {
	ID          string    `json:"id"`
	Name        string    `json:"name"`
	Country     string    `json:"country"`
	Alias       []string  `json:"alias"`
	Coordinates []float64 `json:"coordinates"`
	Province    string    `json:"province"`
	Timezone    string    `json:"timezone"`
	Unlocs      []string  `json:"unlocs"`
	Code        string    `json:"code"`
}

func (p *Port) NewPort(id string, name string, country string, alias []string, coordinates []float64, province string, timezone string, unlocs []string, code string) {
	p.ID = id
	p.Name = name
	p.Country = country
	p.Alias = alias
	p.Coordinates = coordinates
	p.Province = province
	p.Timezone = timezone
	p.Unlocs = unlocs
	p.Code = code
}

func (p *Port) GetID() string {
	return p.ID
}

func (p *Port) Decoder(data []byte) error {
	err := json.Unmarshal(data, p)
	if err != nil {
		return fmt.Errorf("port decoder: %w", err)
	}
	return nil
}
