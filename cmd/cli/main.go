package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"ports/internal/config"
	"ports/internal/constant"
	pg "ports/internal/db/postgres"
	"ports/internal/profile"
	port "ports/pkg/port"
	"sync"
	"syscall"
)

const (
	migrationFolder = "file://migration"
	reset           = true
	fileName        = "ports.json"
	graceful        = false
	concurrent      = true
	writeWorkers    = 25
	logger          = true
)

func main() {
	defer profile.Start("main")() // profiling

	if err := run(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

func run() error {
	// CONFIGURATION
	conf := config.GetConfig()
	log.Print(constant.Green + "LOAD CONFIG" + constant.Reset)

	//  DB CONNECTION
	var singleton sync.Once
	db, err := pg.NewPostgresConnectionPool(&singleton, conf.PostgresHost)
	defer db.Conn.Close()
	if err != nil {
		return err
	}
	if reset {
		err = pg.ResetDB(context.Background(), db.Conn)
		if err != nil {
			return err
		}
	}

	// MIGRATION
	err = pg.Migrate(conf.PostgresHost, migrationFolder, "up", 0)
	if err != nil {
		return err
	}

	// SQL REPOSITORIES
	portRepository := port.NewPGRepository(db.Conn)

	// READ JSON
	json, err := os.Open(fileName)
	defer json.Close()
	if err != nil {
		return err
	}

	// STREAM
	ctx, cancel := context.WithCancel(context.Background())
	wg := &sync.WaitGroup{}
	stream := port.NewPortStream(wg, portRepository, writeWorkers)
	err = stream.DecodeStream(ctx, json, logger, concurrent)
	if err != nil {
		return err
	}

	// GRACEFULL SHUTDOWNS
	if graceful {
		quit := make(chan os.Signal, 1)
		signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
		<-quit
		log.Println("Shutting down ...")
	}

	if concurrent {
		cancel()
		wg.Wait()
	}

	return nil
}
