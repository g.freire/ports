# Port microservices
Microservice responsible for consuming port data from JSON and saving it at a data store.

![Go](https://img.shields.io/badge/go-1.1-%2300ADD8.svg?style=for-the-badge&logo=go&logoColor=white)
![Postgres](https://img.shields.io/badge/postgres-14.2-%23316192.svg?style=for-the-badge&logo=postgresql&logoColor=white)
![Redis](https://img.shields.io/badge/redis-6.2-%23DD0031.svg?style=for-the-badge&logo=redis&logoColor=white)

## Getting Started

This project uses the **Go** programming language (Golang), **PostgreSQL** as the relational database and **Redis** as the cache.

## Steps to run

Clone the repository and enter the folder
```bash
https://gitlab.com/g.freire/ports.git && cd ports
```

##### Debug mode
Compose the local environment (cache + db)
```bash
make local
make stop-local
```
Install, Build and Run Go binary
```go
go run cmd/cli/main.go or
go mod download && go build -o gym cmd/api/main.go && ./gym
```
##### Docker compose network
Compose the dev environment (app + cache + db)
```bash
make dev
make stop-dev
```


### Running the tests

```bash
make test
or
go test ./...
```
#### Creating a migration
```bash
migrate create -ext sql -dir migration -seq some_migration_name
```
#### Deploying an img to registry
```bash
bash deploy-img-to-registry.sh
```

```
TODO:
Improvements on the hard update(change to event sourcing)
Optimize chunk size to infer from source size
Finish Cache Usage
More tests (Unit + Integration + CI)
Web API
Improve docker stages
Kubernetes files
Improvements on closing the concurrent workers (wait group, signals)
Improve dependency injection and config
```