###############################################################
# BUILD STAGE
###############################################################
FROM golang:1.17-buster AS builder
LABEL stage=builder
WORKDIR /port
COPY . .
RUN go build -a -installsuffix cgo -o port ./cmd/cli/
CMD ["./port"]

# ###############################################################
# # DISTRIBUTION STAGE
# ###############################################################
#FROM alpine
#WORKDIR /port/
#COPY ports.json .
#COPY --from=builder ports.json .
#RUN ls
#CMD ["./port"]

