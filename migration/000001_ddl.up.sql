-- DDF
CREATE TABLE IF NOT EXISTS port(
    id VARCHAR(5) NOT NULL,
    name VARCHAR(80) NOT NULL,
    country VARCHAR(60) NOT NULL,
    alias jsonb NULL,
    coordinates jsonb NULL,
    province VARCHAR(60) NOT NULL,
    timezone VARCHAR(80) NOT NULL,
    unlocs jsonb NULL,
    code VARCHAR(80) NULL,
    creation_time TIMESTAMP NOT NULL DEFAULT NOW(),
    version INTEGER NOT NULL DEFAULT 1,
    PRIMARY KEY(id, version)
);