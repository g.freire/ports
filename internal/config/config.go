package config

import (
	"os"

	_ "github.com/joho/godotenv/autoload"
)

const (
	defaultPostgresURILocal = "postgres://port:port@localhost:5432/port?sslmode=disable"
	defaultRedisAddress     = "localhost:6379"
)

type Config struct {
	Environment   string `json:"environment,omitempty"`
	PostgresHost  string `json:"pg_host"`
	RedisAddress  string `json:"redis_address"`
	RedisPassword string `json:"redis_password"`
}

func GetConfig() *Config {
	env := os.Getenv("ENVIRONMENT")
	if env == "" {
		env = "DEV"
	}
	postgresHost := os.Getenv("POSTGRES_HOST")
	if postgresHost == "" {
		postgresHost = defaultPostgresURILocal
	}

	redisAddress := os.Getenv("REDIS_ADDRESS")
	if redisAddress == "" {
		redisAddress = defaultRedisAddress
	}

	return &Config{
		Environment:   env,
		PostgresHost:  postgresHost,
		RedisAddress:  redisAddress,
		RedisPassword: os.Getenv("REDIS_PASSWORD"),
	}
}
