package postgres

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v4/pgxpool"
	"log"
	"ports/internal/constant"
)

func ResetDB(ctx context.Context, db *pgxpool.Pool) error {
	log.Print(constant.Red, "RESET DB", constant.Reset)
	sql := "DROP SCHEMA public CASCADE; CREATE SCHEMA public;"
	if _, err := db.Exec(ctx, sql); err != nil {
		return fmt.Errorf("reset db: %w", err)
	}
	return nil
}
